import java.util.Scanner ;

public class LetterCount
{
  public static void count(String s)
  {
    int[] counter = new int[26];
    int i, index, count, castingLetter ;
    char letter ;

    //Initializing the array
    for (i = 0 ; i < 26 ; i++)
    {
      counter[i] = 0 ;
    }

    for (i = 0 ; i < s.length() ; i++)
    {
      letter = Character.toLowerCase(s.charAt(i)) ;

      //Ignoring the spaces and commas
      if (Character.isAlphabetic(letter))
      {
        /*
        * counter(a) is located in counter[0]
        * As 'a' = 97, the index of a letter is in letter-97
        */
        index = (int) letter - 97 ;
        counter[index]++ ;
      }
    }

    //Displaying the results
    for (i = 0 ; i < 26 ; i++)
    {
      count = counter[i] ;

      if (count > 0)
      {
        castingLetter = i+97 ;
        letter = (char) castingLetter ;
        System.out.println(letter + " : " + Integer.toString(count)) ;
      }
    }
  }

  public static void main(String[] args)
  {
    String sentence ;

    if (args.length == 0)
    {
      System.out.print("Enter a sentence : ") ;
      Scanner sc = new Scanner(System.in);
      sentence = sc.nextLine() ;
    }

    else
    {
      sentence = args[0] ;
    }

    count(sentence) ;
    return ;
  }
}
