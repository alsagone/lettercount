#!/bin/bash

git config --global credential.helper wincred
git config --global user.name "Hakim ABDOUROIHAMANE"
git config --global user.email "hakim_abdou@outlook.com"

git init
git remote add origin https://gitlab.com/alsagone/lettercount.git
git add .
git commit -m "Initial commit"
git push -u origin master
